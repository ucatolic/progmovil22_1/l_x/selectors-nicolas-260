package com.sigmotoa.selectores

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*

var Button1:RadioButton? = null
var Button2:RadioButton? = null
var Button3:RadioButton? = null
var Button4:RadioButton? = null
var Button5:RadioButton? = null
var Button6:RadioButton? = null
var Button7:RadioButton? = null
var Button8:RadioButton? = null

class Activity : AppCompatActivity(), RadioGroup.OnCheckedChangeListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity2)



        val rg:RadioGroup = findViewById(R.id.rg)
        Button1 = findViewById(R.id.radioButton1)
        Button2 = findViewById(R.id.radioButton2)
        Button3 = findViewById(R.id.radioButton3)
        rg?.setOnCheckedChangeListener(this)

        val rg2:RadioGroup = findViewById(R.id.rg2)
        Button4 = findViewById(R.id.radioButton4)
        Button5 = findViewById(R.id.radioButton5)
        Button6 = findViewById(R.id.radioButton6)
        rg2?.setOnCheckedChangeListener(this)

        val rg3:RadioGroup = findViewById(R.id.rg3)
        Button7 = findViewById(R.id.radioButton7)
        Button8 = findViewById(R.id.radioButton8)
        rg3?.setOnCheckedChangeListener(this)

        val rtb:RatingBar =findViewById(R.id.ratingBar2)
        rtb.setOnRatingBarChangeListener { RatingBar, fl, b -> msg("!!$fl Stars rating!!") }

        val btnCargaDatos: Button = findViewById(R.id.back)
        btnCargaDatos.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }

    fun msg (number:String) {
        val tv: TextView = findViewById(R.id.textView)
        tv.setText("$number Selected")
    }

    override fun onCheckedChanged(p0: RadioGroup?, p1: Int) {
        when(p1){
            Button1?.id -> msg("1 button")
            Button2?.id -> msg("2 button")
            Button3?.id -> msg("3 button")
            Button4?.id -> msg("4 button")
            Button5?.id -> msg("5 button")
            Button6?.id -> msg("6 button")
            Button7?.id -> msg("1")
            Button8?.id -> msg("2")
        }
    }

}
